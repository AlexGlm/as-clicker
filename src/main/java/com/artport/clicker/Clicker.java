
package com.artport.clicker;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

final class Clicker {

    final static int ENTER_PRESS_DELAY = 800;
    final static int EVENTS_WAIT_TIMEOUT_SECONDS = 120;

    final static TimerTask ENTER_RUNNER_TASK = new TimerTask() {
        @Override
        public void run() {
            try {
                String command = "powershell.exe Get-Process Chrome | %{ (New-Object -ComObject WScript.Shell).AppActivate($_.MainWindowTitle)}";
                Runtime.getRuntime().exec(command);
                Robot robot = new Robot();
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            } catch (Exception e) {
                System.out.println("Возникла ошибка при эмуляции клавиш: " + e.getMessage());
                cancel();
            }
        }
    };

    public static void downloadDocuments(String path, String password, Date minDate, int limit) throws Exception {

        WebDriver driver = setupDriver(path);

        try {
            driver.manage().window().maximize();
            // Выбор сертификата для входа автоэнтером
            Timer timer = new Timer();
            timer.schedule(ENTER_RUNNER_TASK, 2000, ENTER_PRESS_DELAY);
            driver.manage().timeouts().pageLoadTimeout(EVENTS_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            driver.get("https://as-client.uz.gov.ua/");
            timer.cancel();

            // *[text()='Особистий кабінет']

            WebDriverWait wait = new WebDriverWait(driver, EVENTS_WAIT_TIMEOUT_SECONDS);

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Особистий кабінет']")));
            WebElement cabinetLink = driver.findElement(By.xpath("//*[text()='Особистий кабінет']"));
            cabinetLink.click();

            // Вход
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
            WebElement passwordField = driver.findElement(By.id("password"));
            passwordField.sendKeys(password);
            WebElement passwordSubmitButton = driver.findElement(By.xpath("//input[@value='Увійти']"));
            passwordSubmitButton.click();
            // Ожидагние ссылки на страницу прибытия
            wait.until(ExpectedConditions
                    .visibilityOfElementLocated(By.xpath("//a[text()[contains(.,'Реєстр ПД по прибуттю')]]")));

            // Ждём возможной загрузки диалогового окна
            Thread.sleep(2000);
            try {
                JavascriptExecutor javascriptExecutor;
                if (driver instanceof JavascriptExecutor) {
                    javascriptExecutor = (JavascriptExecutor) driver;
                    javascriptExecutor
                            .executeScript("return document.getElementsByClassName('AjaxML_glasswindow')[0].remove()");
                    javascriptExecutor
                            .executeScript("return document.getElementsByClassName('AjaxML_Dialog')[0].remove()");
                }
            } catch (Exception e) {
                System.err.println(e);
            }

            // Переход на страницу прибытия
            WebElement departureLink = driver.findElement(By.xpath("//a[text()[contains(.,'Реєстр ПД по прибуттю')]]"));
            departureLink.click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ListPDArrive_title")));

            // Очистка фильтра по документам и получение списка
            WebElement updateButton = driver.findElement(By.xpath("//input[@value='Оновити']"));
            WebElement searchButton = driver.findElement(By.xpath("//input[@value='Пошук']"));
            searchButton.click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Очистити']")));

            WebElement сlearSearchButton = driver.findElement(By.xpath("//input[@value='Очистити']"));
            WebElement applySearchButton = driver.findElement(By.xpath("//input[@value='Застосувати']"));
            сlearSearchButton.click();
            applySearchButton.click();
            updateButton.click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("selected")));

            // Загрузка XML
            WebElement downloadLink = driver.findElement(By.xpath("//a[text()[contains(.,'Експорт')]]"));
            WebElement nextButton = driver.findElement(By.xpath("//input[@value='▼']"));
            int counter = 0;
            while (counter++ < limit) {
                WebElement selectedRow = driver.findElement(By.className("selected"));

                // Проверка даты
                String dateString = selectedRow.findElement(By.className("AjaxML_datetime")).getText();
                Date date = new SimpleDateFormat("dd.MM.yyyy").parse(dateString);
                System.out.println("Clicker: EPD #" + counter + " - " + dateString);
                if (date.after(minDate) || date.equals(minDate)) {
                    downloadLink.click();
                    System.out.println("Clicker: Downloading...");
                } else {
                    System.out.println("Clicker: Skip");
                }
                Thread.sleep(300 + new Random().nextInt(1000));
                // Переход к следующему документу
                if (nextButton.isEnabled()) {
                    nextButton.click();
                } else {
                    break;
                }
            }
        } finally {
            // закрытие браузера
            driver.quit();
        }
    }

    private static WebDriver setupDriver(String downloadsPath) throws Exception {
        // Создание пути если не существует и загрузка драйвера
        Files.createDirectories(Paths.get(downloadsPath));

        WebDriverManager.chromedriver().setup();

        // System.setProperty("webdriver.chrome.driver", current + "/chromedriver.exe");
        // Установка драйвера и настроек хрома
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("safebrowsing.enabled", "false");
        prefs.put("profile.default_content_settings.popups", 0);
        prefs.put("download.default_directory", downloadsPath);

        options.addArguments("--ignore-ssl-errors=yes");
        options.addArguments("--ignore-certificate-errors");
        options.setExperimentalOption("prefs", prefs);

        return new ChromeDriver(options);
    }

}