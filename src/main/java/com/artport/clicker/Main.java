package com.artport.clicker;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;

public class Main {
	public static final String VERSION_STRING = "1.0.0";

	public static void main(String[] args) {
		System.out.println("AS-Cicker v" + VERSION_STRING);

		Options options = new Options();

		Option path = new Option("path", true, ""); // путь загрузки (создается если не существует)
		path.setRequired(true);
		options.addOption(path);

		Option password = new Option("password", true, ""); // пароль сертификата пользователя
		password.setRequired(true);
		options.addOption(password);

		Option minDate = new Option("minDate", true, ""); // дата статуса от которой загружать (dd.MM.yyyy)
		minDate.setRequired(true);
		options.addOption(minDate);

		Option limit = new Option("limit", true, ""); // максимальное число просматриваемых документов
		limit.setRequired(true);
		options.addOption(limit);

		CommandLineParser parser = new DefaultParser();
		ScreenRecorder screenRecorder = null;

		try {
			GraphicsConfiguration gc = GraphicsEnvironment
					.getLocalGraphicsEnvironment()
					.getDefaultScreenDevice()
					.getDefaultConfiguration();

			String jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation()
					.toURI()).getParent();

			Files.createDirectories(Paths.get(jarPath + "/asclickerlogs"));

			screenRecorder = new ScreenRecorder(gc,
					gc.getBounds(),
					new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
					new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
							CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
							DepthKey, 24, FrameRateKey, Rational.valueOf(15),
							QualityKey, 1.0f,
							KeyFrameIntervalKey, 15 * 60),
					new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black", FrameRateKey, Rational.valueOf(30)),
					null,
					new File(jarPath + "/asclickerlogs"));

			screenRecorder.start();
			System.out.println("Processing...");
			CommandLine cmd = parser.parse(options, args);

			Clicker.downloadDocuments(cmd.getOptionValue("path"), cmd.getOptionValue("password"),
					new SimpleDateFormat("dd.MM.yyyy").parse(cmd.getOptionValue("minDate")),
					Integer.parseInt(cmd.getOptionValue("limit")));

			screenRecorder.stop();
			System.out.println("Done");
			System.exit(0);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			if (screenRecorder != null) {
				try {
					screenRecorder.stop();
				} catch (IOException exception) {
					System.out.println(exception.getMessage());
				}
			}
			System.exit(1);
		}
	}

}
